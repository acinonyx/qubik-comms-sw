options:
  parameters:
    author: ''
    category: '[GRC Hier Blocks]'
    cmake_opt: ''
    comment: ''
    copyright: ''
    description: ''
    gen_cmake: 'On'
    gen_linking: dynamic
    generate_options: qt_gui
    hier_block_src_path: '.:'
    id: qubik_fsk_transmitter
    max_nouts: '0'
    output_language: python
    placement: (0,0)
    qt_qss_theme: ''
    realtime_scheduling: ''
    run: 'True'
    run_command: '{python} -u {filename}'
    run_options: prompt
    sizing_mode: fixed
    thread_safe_setters: ''
    title: FSK Transmitter for QUBIK
    window_size: ''
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [8, 8]
    rotation: 0
    state: enabled

blocks:
- name: baudrate
  id: variable
  parameters:
    comment: Samples per symbol
    value: '9600'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [504, 12.0]
    rotation: 0
    state: true
- name: deviation
  id: variable
  parameters:
    comment: The FSK frequency deviation
    value: 4.8e3
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [608, 12.0]
    rotation: 0
    state: enabled
- name: freq_rx
  id: variable_qtgui_range
  parameters:
    comment: ''
    gui_hint: ''
    label: ''
    min_len: '200'
    orient: Qt.Horizontal
    rangeType: float
    start: 400e6
    step: '100'
    stop: 500e6
    value: 433.2e6
    widget: counter_slider
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [96, 316.0]
    rotation: 0
    state: enabled
- name: freq_tx
  id: variable_qtgui_range
  parameters:
    comment: ''
    gui_hint: ''
    label: ''
    min_len: '200'
    orient: Qt.Horizontal
    rangeType: float
    start: 400e6
    step: '100'
    stop: 500e6
    value: 433.2e6
    widget: counter_slider
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1384, 540.0]
    rotation: 0
    state: enabled
- name: gain_rx
  id: variable_qtgui_range
  parameters:
    comment: ''
    gui_hint: ''
    label: ''
    min_len: '200'
    orient: Qt.Horizontal
    rangeType: float
    start: '-12'
    step: '0.5'
    stop: '89'
    value: '0'
    widget: counter_slider
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [224, 316.0]
    rotation: 0
    state: enabled
- name: gain_tx
  id: variable_qtgui_range
  parameters:
    comment: ''
    gui_hint: ''
    label: ''
    min_len: '200'
    orient: Qt.Horizontal
    rangeType: float
    start: '-12'
    step: '0.5'
    stop: '89'
    value: '0'
    widget: counter_slider
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1520, 540.0]
    rotation: 0
    state: enabled
- name: gaussian_taps
  id: variable
  parameters:
    comment: ''
    value: filter.firdes.gaussian(1.0, sps, 1.0, 4*sps)
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [8, 764.0]
    rotation: 0
    state: enabled
- name: interp_taps
  id: variable
  parameters:
    comment: ''
    value: numpy.convolve(numpy.array(gaussian_taps), numpy.array(sq_wave))
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [8, 892.0]
    rotation: 0
    state: enabled
- name: lo
  id: variable
  parameters:
    comment: ''
    value: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [288, 12.0]
    rotation: 0
    state: enabled
- name: modulation_index
  id: variable
  parameters:
    comment: ''
    value: deviation / (baudrate / 2.0)
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [8, 700.0]
    rotation: 0
    state: enabled
- name: nfilts
  id: variable
  parameters:
    comment: ''
    value: '32'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [376, 932.0]
    rotation: 0
    state: enabled
- name: rrc_taps
  id: variable
  parameters:
    comment: ''
    value: firdes.root_raised_cosine(nfilts, nfilts, 1.0/float(sps_rx), excess_bw,
      11*sps_rx*nfilts)
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [560, 932.0]
    rotation: 0
    state: enabled
- name: samp_rate
  id: variable
  parameters:
    comment: ''
    value: 1e6
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [184, 12]
    rotation: 0
    state: enabled
- name: sps
  id: variable
  parameters:
    comment: Samples per symbol
    value: '32'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [384, 12.0]
    rotation: 0
    state: true
- name: sps_rx
  id: variable
  parameters:
    comment: Samples per symbol
    value: '4'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [960, 12.0]
    rotation: 0
    state: true
- name: sq_wave
  id: variable
  parameters:
    comment: ''
    value: (1.0, ) * sps
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [8, 828.0]
    rotation: 0
    state: enabled
- name: variable_ax25_decoder_0
  id: variable_ax25_decoder
  parameters:
    addr: '''GND'''
    comment: ''
    crc_check: 'True'
    descrambling: 'True'
    frame_len: '1024'
    promisc: 'True'
    ssid: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1328, 1140.0]
    rotation: 180
    state: disabled
- name: variable_ax25_decoder_0_0
  id: variable_ax25_decoder
  parameters:
    addr: '''GND'''
    comment: ''
    crc_check: 'True'
    descrambling: 'False'
    frame_len: '1024'
    promisc: 'True'
    ssid: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1136, 1116.0]
    rotation: 180
    state: disabled
- name: variable_constellation_0
  id: variable_constellation
  parameters:
    comment: ''
    const_points: '[-1-1j, -1+1j, 1+1j, 1-1j]'
    dims: '1'
    precision: '8'
    rot_sym: '4'
    soft_dec_lut: None
    sym_map: '[0, 1, 3, 2]'
    type: bpsk
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [928, 1092.0]
    rotation: 0
    state: disabled
- name: variable_ieee802_15_4_encoder_0
  id: variable_ieee802_15_4_encoder
  parameters:
    comment: ''
    crc: satnogs.crc.CRC16_CCITT
    preamble: '0x55'
    preamble_len: '64'
    sync_word: '[49, 229]'
    var_len: 'True'
    whitening: variable_whitening_ccsds_0
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [760, 12.0]
    rotation: 0
    state: true
- name: variable_whitening_ccsds_0
  id: variable_whitening_ccsds
  parameters:
    comment: ''
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [760, 156.0]
    rotation: 0
    state: true
- name: analog_agc2_xx_0
  id: analog_agc2_xx
  parameters:
    affinity: ''
    alias: ''
    attack_rate: 1e-2
    comment: ''
    decay_rate: 1e-4
    gain: '1.0'
    max_gain: '65536'
    maxoutbuf: '0'
    minoutbuf: '0'
    reference: '0.5'
    type: complex
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [376, 772.0]
    rotation: 0
    state: enabled
- name: analog_frequency_modulator_fc_0
  id: analog_frequency_modulator_fc
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    sensitivity: (math.pi*modulation_index) / sps
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1032, 252.0]
    rotation: 0
    state: enabled
- name: analog_quadrature_demod_cf_0
  id: analog_quadrature_demod_cf
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    gain: '1'
    maxoutbuf: '0'
    minoutbuf: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [944, 764.0]
    rotation: 0
    state: enabled
- name: analog_sig_source_x_0
  id: analog_sig_source_x
  parameters:
    affinity: ''
    alias: ''
    amp: '1'
    comment: ''
    freq: -lo
    maxoutbuf: '0'
    minoutbuf: '0'
    offset: '0'
    phase: '0'
    samp_rate: samp_rate
    type: complex
    waveform: analog.GR_COS_WAVE
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [176, 644.0]
    rotation: 0
    state: enabled
- name: analog_sig_source_x_0_0
  id: analog_sig_source_x
  parameters:
    affinity: ''
    alias: ''
    amp: '1'
    comment: ''
    freq: lo
    maxoutbuf: '0'
    minoutbuf: '0'
    offset: '0'
    phase: '0'
    samp_rate: samp_rate
    type: complex
    waveform: analog.GR_COS_WAVE
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1776, 460.0]
    rotation: 180
    state: enabled
- name: blocks_message_strobe_random_0
  id: blocks_message_strobe_random
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    dist: blocks.STROBE_UNIFORM
    maxoutbuf: '0'
    mean: '200'
    minoutbuf: '0'
    msg: pmt.intern("TEST")
    std: '100'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [320, 124.0]
    rotation: 180
    state: true
- name: blocks_multiply_xx_0
  id: blocks_multiply_xx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    num_inputs: '2'
    type: complex
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [384, 536.0]
    rotation: 0
    state: enabled
- name: blocks_multiply_xx_0_0
  id: blocks_multiply_xx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    num_inputs: '2'
    type: complex
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1632, 456.0]
    rotation: 180
    state: enabled
- name: blocks_null_sink_0
  id: blocks_null_sink
  parameters:
    affinity: ''
    alias: ''
    bus_structure_sink: '[[0,],]'
    comment: ''
    num_inputs: '3'
    type: float
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1400, 952.0]
    rotation: 0
    state: disabled
- name: blocks_packed_to_unpacked_xx_0
  id: blocks_packed_to_unpacked_xx
  parameters:
    affinity: ''
    alias: ''
    bits_per_chunk: '1'
    comment: ''
    endianness: gr.GR_MSB_FIRST
    maxoutbuf: '0'
    minoutbuf: '0'
    num_ports: '1'
    type: byte
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [488, 244.0]
    rotation: 0
    state: true
- name: blocks_pdu_to_tagged_stream_0
  id: blocks_pdu_to_tagged_stream
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    tag: packet_len
    type: byte
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [272, 252.0]
    rotation: 0
    state: enabled
- name: blocks_random_pdu_0
  id: blocks_random_pdu
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    length_modulo: '2'
    mask: '0xFF'
    maxoutbuf: '0'
    maxsize: '250'
    minoutbuf: '0'
    minsize: '250'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [96, 124.0]
    rotation: 180
    state: true
- name: blocks_tagged_stream_multiply_length_0
  id: blocks_tagged_stream_multiply_length
  parameters:
    affinity: ''
    alias: ''
    c: sps * 8
    comment: ''
    lengthtagname: packet_len
    maxoutbuf: '0'
    minoutbuf: '0'
    type: complex
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1200, 256.0]
    rotation: 0
    state: true
- name: dc_blocker_xx_0
  id: dc_blocker_xx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    length: '1024'
    long_form: 'True'
    maxoutbuf: '0'
    minoutbuf: '0'
    type: ff
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1096, 756.0]
    rotation: 0
    state: enabled
- name: digital_burst_shaper_xx_0
  id: digital_burst_shaper_xx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    insert_phasing: 'False'
    length_tag_name: '"packet_len"'
    maxoutbuf: '0'
    minoutbuf: '0'
    post_padding: '100'
    pre_padding: '0'
    type: complex
    window: ([])
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1456, 236.0]
    rotation: 0
    state: bypassed
- name: digital_chunks_to_symbols_xx_0
  id: digital_chunks_to_symbols_xx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    dimension: '1'
    in_type: byte
    maxoutbuf: '0'
    minoutbuf: '0'
    num_ports: '1'
    out_type: float
    symbol_table: '[-1, 1]'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [664, 256.0]
    rotation: 0
    state: true
- name: digital_clock_recovery_mm_xx_0
  id: digital_clock_recovery_mm_xx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    gain_mu: '0.175'
    gain_omega: 0.25*0.175*0.175
    maxoutbuf: '0'
    minoutbuf: '0'
    mu: '0.5'
    omega: sps_rx
    omega_relative_limit: '0.005'
    type: float
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1256, 732.0]
    rotation: 0
    state: enabled
- name: digital_constellation_receiver_cb_0
  id: digital_constellation_receiver_cb
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    constellation: variable_constellation_0
    fmax: '1'
    fmin: '-1'
    loop_bw: 2*math.pi/100.0
    maxoutbuf: '0'
    minoutbuf: '0'
    showports: 'False'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1144, 904.0]
    rotation: 0
    state: disabled
- name: digital_pfb_clock_sync_xxx_0
  id: digital_pfb_clock_sync_xxx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    filter_size: nfilts
    init_phase: nfilts/2
    loop_bw: 2.0 * math.pi/100.0
    max_dev: '1.5'
    maxoutbuf: '0'
    minoutbuf: '0'
    osps: '1'
    sps: sps_rx
    taps: rrc_taps
    type: ccf
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [880, 932.0]
    rotation: 0
    state: disabled
- name: excess_bw
  id: parameter
  parameters:
    alias: ''
    comment: 'The BPSK excess bandwidth setup.

      Used in filtering.'
    hide: none
    label: ''
    short_id: ''
    type: eng_float
    value: '0.5'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [448, 924.0]
    rotation: 0
    state: enabled
- name: import_0
  id: import
  parameters:
    alias: ''
    comment: ''
    imports: import numpy
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [8, 956.0]
    rotation: 0
    state: true
- name: import_0_0
  id: import
  parameters:
    alias: ''
    comment: ''
    imports: import math
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [8, 1004.0]
    rotation: 0
    state: true
- name: interp_fir_filter_xxx_0
  id: interp_fir_filter_xxx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    interp: sps
    maxoutbuf: '0'
    minoutbuf: '0'
    samp_delay: '0'
    taps: interp_taps
    type: fff
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [848, 244.0]
    rotation: 0
    state: enabled
- name: low_pass_filter_0
  id: low_pass_filter
  parameters:
    affinity: ''
    alias: ''
    beta: '6.76'
    comment: ''
    cutoff_freq: baudrate * 0.6
    decim: '1'
    gain: '1'
    interp: '1'
    maxoutbuf: '0'
    minoutbuf: '0'
    samp_rate: baudrate*4
    type: fir_filter_ccf
    width: '1000'
    win: firdes.WIN_HAMMING
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [744, 716.0]
    rotation: 0
    state: enabled
- name: pfb_arb_resampler_xxx_0_0
  id: pfb_arb_resampler_xxx
  parameters:
    affinity: ''
    alias: ''
    atten: '100'
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    nfilts: '32'
    rrate: (baudrate*sps_rx)/(samp_rate)
    samp_delay: '0'
    taps: ''
    type: ccf
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [536, 780.0]
    rotation: 0
    state: enabled
- name: pfb_arb_resampler_xxx_0_0_0
  id: pfb_arb_resampler_xxx
  parameters:
    affinity: ''
    alias: ''
    atten: '100'
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    nfilts: '32'
    rrate: samp_rate/(baudrate*sps)
    samp_delay: '0'
    taps: ''
    type: ccf
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1664, 244.0]
    rotation: 0
    state: enabled
- name: qtgui_const_sink_x_0
  id: qtgui_const_sink_x
  parameters:
    affinity: ''
    alias: ''
    alpha1: '1.0'
    alpha10: '1.0'
    alpha2: '1.0'
    alpha3: '1.0'
    alpha4: '1.0'
    alpha5: '1.0'
    alpha6: '1.0'
    alpha7: '1.0'
    alpha8: '1.0'
    alpha9: '1.0'
    autoscale: 'False'
    axislabels: 'True'
    color1: '"blue"'
    color10: '"red"'
    color2: '"red"'
    color3: '"red"'
    color4: '"red"'
    color5: '"red"'
    color6: '"red"'
    color7: '"red"'
    color8: '"red"'
    color9: '"red"'
    comment: ''
    grid: 'False'
    gui_hint: ''
    label1: ''
    label10: ''
    label2: ''
    label3: ''
    label4: ''
    label5: ''
    label6: ''
    label7: ''
    label8: ''
    label9: ''
    legend: 'True'
    marker1: '0'
    marker10: '0'
    marker2: '0'
    marker3: '0'
    marker4: '0'
    marker5: '0'
    marker6: '0'
    marker7: '0'
    marker8: '0'
    marker9: '0'
    name: '""'
    nconnections: '1'
    size: '1024'
    style1: '0'
    style10: '0'
    style2: '0'
    style3: '0'
    style4: '0'
    style5: '0'
    style6: '0'
    style7: '0'
    style8: '0'
    style9: '0'
    tr_chan: '0'
    tr_level: '0.0'
    tr_mode: qtgui.TRIG_MODE_FREE
    tr_slope: qtgui.TRIG_SLOPE_POS
    tr_tag: '""'
    type: complex
    update_time: '0.10'
    width1: '1'
    width10: '1'
    width2: '1'
    width3: '1'
    width4: '1'
    width5: '1'
    width6: '1'
    width7: '1'
    width8: '1'
    width9: '1'
    xmax: '2'
    xmin: '-2'
    ymax: '2'
    ymin: '-2'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1384, 1068.0]
    rotation: 0
    state: disabled
- name: qtgui_freq_sink_x_0_0
  id: qtgui_freq_sink_x
  parameters:
    affinity: ''
    alias: ''
    alpha1: '1.0'
    alpha10: '1.0'
    alpha2: '1.0'
    alpha3: '1.0'
    alpha4: '1.0'
    alpha5: '1.0'
    alpha6: '1.0'
    alpha7: '1.0'
    alpha8: '1.0'
    alpha9: '1.0'
    autoscale: 'False'
    average: '1.0'
    axislabels: 'True'
    bw: samp_rate
    color1: '"blue"'
    color10: '"dark blue"'
    color2: '"red"'
    color3: '"green"'
    color4: '"black"'
    color5: '"cyan"'
    color6: '"magenta"'
    color7: '"yellow"'
    color8: '"dark red"'
    color9: '"dark green"'
    comment: ''
    ctrlpanel: 'False'
    fc: '0'
    fftsize: '1024'
    freqhalf: 'True'
    grid: 'False'
    gui_hint: ''
    label: Relative Gain
    label1: ''
    label10: ''''''
    label2: ''''''
    label3: ''''''
    label4: ''''''
    label5: ''''''
    label6: ''''''
    label7: ''''''
    label8: ''''''
    label9: ''''''
    legend: 'True'
    maxoutbuf: '0'
    minoutbuf: '0'
    name: '""'
    nconnections: '1'
    showports: 'False'
    tr_chan: '0'
    tr_level: '0.0'
    tr_mode: qtgui.TRIG_MODE_FREE
    tr_tag: '""'
    type: complex
    units: dB
    update_time: '0.010'
    width1: '1'
    width10: '1'
    width2: '1'
    width3: '1'
    width4: '1'
    width5: '1'
    width6: '1'
    width7: '1'
    width8: '1'
    width9: '1'
    wintype: firdes.WIN_BLACKMAN_hARRIS
    ymax: '10'
    ymin: '-140'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [568, 552.0]
    rotation: 0
    state: enabled
- name: qtgui_time_sink_x_0
  id: qtgui_time_sink_x
  parameters:
    affinity: ''
    alias: ''
    alpha1: '1.0'
    alpha10: '1.0'
    alpha2: '1.0'
    alpha3: '1.0'
    alpha4: '1.0'
    alpha5: '1.0'
    alpha6: '1.0'
    alpha7: '1.0'
    alpha8: '1.0'
    alpha9: '1.0'
    autoscale: 'False'
    axislabels: 'True'
    color1: blue
    color10: dark blue
    color2: red
    color3: green
    color4: black
    color5: cyan
    color6: magenta
    color7: yellow
    color8: dark red
    color9: dark green
    comment: ''
    ctrlpanel: 'False'
    entags: 'True'
    grid: 'False'
    gui_hint: ''
    label1: Signal 1
    label10: Signal 10
    label2: Signal 2
    label3: Signal 3
    label4: Signal 4
    label5: Signal 5
    label6: Signal 6
    label7: Signal 7
    label8: Signal 8
    label9: Signal 9
    legend: 'True'
    marker1: '-1'
    marker10: '-1'
    marker2: '-1'
    marker3: '-1'
    marker4: '-1'
    marker5: '-1'
    marker6: '-1'
    marker7: '-1'
    marker8: '-1'
    marker9: '-1'
    name: '""'
    nconnections: '1'
    size: '1024'
    srate: samp_rate
    stemplot: 'False'
    style1: '1'
    style10: '1'
    style2: '1'
    style3: '1'
    style4: '1'
    style5: '1'
    style6: '1'
    style7: '1'
    style8: '1'
    style9: '1'
    tr_chan: '0'
    tr_delay: '0'
    tr_level: '0.0'
    tr_mode: qtgui.TRIG_MODE_FREE
    tr_slope: qtgui.TRIG_SLOPE_POS
    tr_tag: '""'
    type: float
    update_time: '0.10'
    width1: '1'
    width10: '1'
    width2: '1'
    width3: '1'
    width4: '1'
    width5: '1'
    width6: '1'
    width7: '1'
    width8: '1'
    width9: '1'
    ylabel: Amplitude
    ymax: '1'
    ymin: '-1'
    yunit: '""'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1456, 748.0]
    rotation: 0
    state: enabled
- name: satnogs_frame_decoder_0_0
  id: satnogs_frame_decoder
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    decoder_object: variable_ax25_decoder_0
    itype: byte
    maxoutbuf: '0'
    minoutbuf: '0'
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1584, 1064.0]
    rotation: 0
    state: disabled
- name: satnogs_frame_decoder_0_0_0
  id: satnogs_frame_decoder
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    decoder_object: variable_ax25_decoder_0_0
    itype: byte
    maxoutbuf: '0'
    minoutbuf: '0'
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1552, 840.0]
    rotation: 0
    state: disabled
- name: satnogs_frame_encoder_0
  id: satnogs_frame_encoder
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    encoder_object: variable_ieee802_15_4_encoder_0
    maxoutbuf: '0'
    minoutbuf: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [48, 252.0]
    rotation: 0
    state: true
- name: satnogs_json_converter_0
  id: satnogs_json_converter
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    extra: ''
    maxoutbuf: '0'
    minoutbuf: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1664, 972.0]
    rotation: 0
    state: disabled
- name: satnogs_multi_format_msg_sink_0
  id: satnogs_multi_format_msg_sink
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    filepath: ''
    format: '0'
    outstream: 'True'
    timestamp: 'False'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1832, 956.0]
    rotation: 0
    state: disabled
- name: soapy_sink_0
  id: soapy_sink
  parameters:
    affinity: ''
    alias: ''
    amp_gain0: '0'
    ant0: TX/RX
    ant1: TX
    args: ''
    balance0: '0'
    balance1: '0'
    bw0: '0'
    bw1: '0'
    center_freq0: freq_tx-lo
    center_freq1: 100.0e6
    clock_rate: '0'
    clock_source: ''
    comment: ''
    correction0: '0'
    correction1: '0'
    dc_offset0: '0'
    dc_offset1: '0'
    dc_offset_auto_mode0: 'False'
    dc_offset_auto_mode1: 'False'
    dev: driver=uhd
    devname: uhd
    gain_auto_mode0: 'False'
    gain_auto_mode1: 'False'
    iamp_gain0: '0'
    iamp_gain1: '0'
    length_tag_name: ''
    manual_gain0: 'False'
    manual_gain1: 'True'
    nchan: '1'
    nco_freq0: '0'
    nco_freq1: '0'
    overall_gain0: gain_tx
    overall_gain1: '0'
    pad_gain0: '0'
    pad_gain1: '0'
    pga_gain0: gain_tx
    pga_gain1: '0'
    samp_rate: samp_rate
    type: fc32
    vga_gain0: '10'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1384, 404.0]
    rotation: 180
    state: enabled
- name: soapy_source_1
  id: soapy_source
  parameters:
    affinity: ''
    alias: ''
    amp_gain0: '0'
    ant0: RX2
    ant1: RX2
    args: ''
    balance0: '0'
    balance1: '0'
    bw0: '0'
    bw1: '0'
    center_freq0: freq_rx+lo
    center_freq1: '0'
    clock_rate: '0'
    clock_source: ''
    comment: ''
    correction0: '0'
    correction1: '0'
    dc_offset0: '0'
    dc_offset1: '0'
    dc_offset_auto_mode0: 'False'
    dc_offset_auto_mode1: 'False'
    dev: driver=rtlsdr
    devname: uhd
    gain_auto_mode0: 'False'
    gain_auto_mode1: 'False'
    ifgr_gain: '59'
    lna_gain0: '10'
    lna_gain1: '10'
    manual_gain0: 'False'
    manual_gain1: 'True'
    maxoutbuf: '0'
    minoutbuf: '0'
    mix_gain0: '10'
    mix_gain1: '10'
    nchan: '1'
    nco_freq0: '0'
    nco_freq1: '0'
    overall_gain0: '30'
    overall_gain1: '10'
    pga_gain0: '24'
    pga_gain1: '24'
    rfgr_gain: '9'
    samp_rate: samp_rate
    sdrplay_agc_setpoint: '-30'
    sdrplay_biastee: 'False'
    sdrplay_dabnotch: 'False'
    sdrplay_if_mode: Zero-IF
    sdrplay_rfnotch: 'False'
    tia_gain0: '0'
    tia_gain1: '0'
    tuner_gain0: '10'
    tuner_gain1: '10'
    type: fc32
    vga_gain0: '10'
    vga_gain1: '10'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [120, 508.0]
    rotation: 0
    state: true

connections:
- [analog_agc2_xx_0, '0', pfb_arb_resampler_xxx_0_0, '0']
- [analog_frequency_modulator_fc_0, '0', blocks_tagged_stream_multiply_length_0, '0']
- [analog_quadrature_demod_cf_0, '0', dc_blocker_xx_0, '0']
- [analog_sig_source_x_0, '0', blocks_multiply_xx_0, '1']
- [analog_sig_source_x_0_0, '0', blocks_multiply_xx_0_0, '1']
- [blocks_message_strobe_random_0, strobe, blocks_random_pdu_0, generate]
- [blocks_multiply_xx_0, '0', analog_agc2_xx_0, '0']
- [blocks_multiply_xx_0, '0', qtgui_freq_sink_x_0_0, '0']
- [blocks_multiply_xx_0_0, '0', soapy_sink_0, '0']
- [blocks_packed_to_unpacked_xx_0, '0', digital_chunks_to_symbols_xx_0, '0']
- [blocks_pdu_to_tagged_stream_0, '0', blocks_packed_to_unpacked_xx_0, '0']
- [blocks_random_pdu_0, pdus, satnogs_frame_encoder_0, pdu]
- [blocks_tagged_stream_multiply_length_0, '0', digital_burst_shaper_xx_0, '0']
- [dc_blocker_xx_0, '0', digital_clock_recovery_mm_xx_0, '0']
- [digital_burst_shaper_xx_0, '0', pfb_arb_resampler_xxx_0_0_0, '0']
- [digital_chunks_to_symbols_xx_0, '0', interp_fir_filter_xxx_0, '0']
- [digital_clock_recovery_mm_xx_0, '0', qtgui_time_sink_x_0, '0']
- [digital_constellation_receiver_cb_0, '0', satnogs_frame_decoder_0_0, '0']
- [digital_constellation_receiver_cb_0, '0', satnogs_frame_decoder_0_0_0, '0']
- [digital_constellation_receiver_cb_0, '1', blocks_null_sink_0, '0']
- [digital_constellation_receiver_cb_0, '2', blocks_null_sink_0, '1']
- [digital_constellation_receiver_cb_0, '3', blocks_null_sink_0, '2']
- [digital_constellation_receiver_cb_0, '4', qtgui_const_sink_x_0, '0']
- [digital_pfb_clock_sync_xxx_0, '0', digital_constellation_receiver_cb_0, '0']
- [interp_fir_filter_xxx_0, '0', analog_frequency_modulator_fc_0, '0']
- [low_pass_filter_0, '0', analog_quadrature_demod_cf_0, '0']
- [low_pass_filter_0, '0', digital_pfb_clock_sync_xxx_0, '0']
- [pfb_arb_resampler_xxx_0_0, '0', low_pass_filter_0, '0']
- [pfb_arb_resampler_xxx_0_0_0, '0', blocks_multiply_xx_0_0, '0']
- [satnogs_frame_decoder_0_0, out, satnogs_json_converter_0, in]
- [satnogs_frame_decoder_0_0_0, out, satnogs_json_converter_0, in]
- [satnogs_frame_encoder_0, pdu, blocks_pdu_to_tagged_stream_0, pdus]
- [satnogs_json_converter_0, out, satnogs_multi_format_msg_sink_0, in]
- [soapy_source_1, '0', blocks_multiply_xx_0, '0']

metadata:
  file_format: 1
