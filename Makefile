# 
# Qubik: An open source 5x5x5 pico-satellite
#
# Copyright (C) 2020, Libre Space Foundation <http://libre.space>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

######################################
# target
######################################
TARGET = qubik-comms-sw


######################################
# building variables
######################################
# debug build?
ifeq ($(QEMU), 1)
DEBUG = 1
else
DEBUG = 0
endif

# Optimization flags
ifeq ($(DEBUG), 1)
OPT = -Og -g -gdwarf-2
else
OPT = -Os
endif


#######################################
# paths
#######################################
# Build path
ifeq ($(QEMU), 1)
BUILD_DIR = build-qemu
else
BUILD_DIR = build
endif

######################################
# source
######################################
# C sources
C_SOURCES =  \
Core/Src/main.c \
Core/Src/bsp_pq9ish_comms.c \
Core/Src/crc.c \
Core/Src/experiment.c \
Core/Src/freertos.c \
Core/Src/ft_storage.c \
Core/Src/lfsr.c \
Core/Src/stm32l4xx_it.c \
Core/Src/stm32l4xx_hal_msp.c \
Core/Src/stm32l4xx_hal_timebase_tim.c \
Core/Src/ax5043_driver.c \
Core/Src/radio.c \
Core/Src/rildos.c \
Core/Src/test.c \
Core/Src/qubik.c \
Core/Src/queue_util.c \
Core/Src/osdlp_queue_handle.c \
Core/Src/watchdog.c \
Core/Src/max17261_driver.c \
Core/Src/power.c \
Core/Src/antenna.c \
Core/Src/fsm.c \
Core/Src/telemetry.c \
Core/Src/telecommand.c \
Core/Src/sha256.c \
Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_adc.c \
Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_adc_ex.c \
Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_crc.c \
Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_crc_ex.c \
Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_rtc.c \
Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_rtc_ex.c \
Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_spi.c \
Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_spi_ex.c \
Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_tim.c \
Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_tim_ex.c \
Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal.c \
Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_i2c.c \
Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_i2c_ex.c \
Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_iwdg.c \
Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_rcc.c \
Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_rcc_ex.c \
Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_flash.c \
Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_flash_ex.c \
Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_flash_ramfunc.c \
Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_gpio.c \
Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_dma.c \
Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_dma_ex.c \
Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_pwr.c \
Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_pwr_ex.c \
Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_cortex.c \
Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_exti.c \
Drivers/AX5043/src/ax5043.c \
Drivers/AX5043/src/fec.c \
Drivers/AX5043/src/ccsds.c \
Drivers/OSDLP/src/osdlp_tm.c \
Drivers/OSDLP/src/osdlp_tc.c \
Drivers/OSDLP/src/osdlp_crc.c \
Drivers/OSDLP/src/osdlp_cop.c \
Drivers/OSDLP/src/osdlp_clcw.c \
Drivers/MAX17261/src/max17261.c \
Core/Src/system_stm32l4xx.c \
Middlewares/Third_Party/FreeRTOS/Source/croutine.c \
Middlewares/Third_Party/FreeRTOS/Source/event_groups.c \
Middlewares/Third_Party/FreeRTOS/Source/list.c \
Middlewares/Third_Party/FreeRTOS/Source/queue.c \
Middlewares/Third_Party/FreeRTOS/Source/stream_buffer.c \
Middlewares/Third_Party/FreeRTOS/Source/tasks.c \
Middlewares/Third_Party/FreeRTOS/Source/timers.c \
Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS/cmsis_os.c \
Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F/port.c

C_SOURCES := $(filter-out Drivers/OSDLP/test/*.c, $(C_SOURCES))
C_SOURCES := $(filter-out Drivers/OSDLP/test/*.h, $(C_SOURCES))

# ASM sources
ASM_SOURCES =  \
Core/Startup/startup_stm32l476rgtx.s


#######################################
# binaries
#######################################
PREFIX = arm-none-eabi-
# The gcc compiler bin path can be either defined in make command via GCC_PATH variable (> make GCC_PATH=xxx)
# either it can be added to the PATH environment variable.
ifdef GCC_PATH
CC = $(GCC_PATH)/$(PREFIX)gcc
AS = $(GCC_PATH)/$(PREFIX)gcc -x assembler-with-cpp
CP = $(GCC_PATH)/$(PREFIX)objcopy
SZ = $(GCC_PATH)/$(PREFIX)size
else
CC = $(PREFIX)gcc
AS = $(PREFIX)gcc -x assembler-with-cpp
CP = $(PREFIX)objcopy
SZ = $(PREFIX)size
endif
HEX = $(CP) -O ihex
BIN = $(CP) -O binary -S

#######################################
# CFLAGS
#######################################
# cpu
CPU = -mcpu=cortex-m4

# fpu
FPU = -mfpu=fpv4-sp-d16

# float-abi
FLOAT-ABI = -mfloat-abi=hard

# mcu
MCU = $(CPU)

 # Instruction set flags
INSTR_SET_FLAGS = $(FPU) $(FLOAT-ABI) -mthumb

# macros for gcc
# AS defines
AS_DEFS =

MAX17261_USE_WEAK := 1

# C defines
C_DEFS =  \
-DUSE_HAL_DRIVER \
-DSTM32L476xx

ifeq ($(MAX17261_USE_WEAK), 1)
C_DEFS += -DMAX17261_USE_WEAK
endif

ifeq ($(QEMU), 1)
C_DEFS += -DQEMU
endif

# AS includes
AS_INCLUDES =  \
-I/Core/Inc

# C includes
C_INCLUDES =  \
-ICore/Inc \
-I. \
-IDrivers/STM32L4xx_HAL_Driver/Inc \
-IDrivers/STM32L4xx_HAL_Driver/Inc/Legacy \
-IMiddlewares/Third_Party/FatFs/src \
-IMiddlewares/Third_Party/FreeRTOS/Source/include \
-IMiddlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS \
-IMiddlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F \
-IDrivers/CMSIS/Device/ST/STM32L4xx/Include \
-IDrivers/CMSIS/Include \
-IDrivers/AX5043/include \
-IDrivers/OSDLP/include \
-IDrivers/MAX17261/include

# Generate dependency information
DEPS = -MMD -MP -MF"$(@:%.o=%.d)"

# compile gcc flags
override CFLAGS += $(MCU) $(C_DEFS) $(C_INCLUDES) $(DEPS) --specs=nano.specs $(INSTR_SET_FLAGS) $(OPT) -Wall -fdata-sections -ffunction-sections -std=gnu11

#######################################
# LDFLAGS
#######################################
# link script
LDSCRIPT = STM32L476RGTx_FLASH.ld

# libraries
LIBS = -lc -lm -lnosys
LDFLAGS = $(MCU) -T$(BUILD_DIR)/$(LDSCRIPT) --specs=nosys.specs -Wl,-Map=$(BUILD_DIR)/$(TARGET).map -Wl,--gc-sections -static --specs=nano.specs $(INSTR_SET_FLAGS) -Wl,--start-group $(LIBS) -Wl,--end-group

#######################################
# programming options (openocd)
#######################################
OPENOCD_FLAGS =
OPENOCD_OPTIONS =
OPENOCD = openocd

# default action: build all
all: $(BUILD_DIR)/$(TARGET).elf $(BUILD_DIR)/$(TARGET).hex $(BUILD_DIR)/$(TARGET).bin

#######################################
# build the application
#######################################
# list of objects
OBJECTS = $(addprefix $(BUILD_DIR)/,$(notdir $(C_SOURCES:.c=.o)))
vpath %.c $(sort $(dir $(C_SOURCES)))
# list of ASM program objects
OBJECTS += $(addprefix $(BUILD_DIR)/,$(notdir $(ASM_SOURCES:.s=.o)))
vpath %.s $(sort $(dir $(ASM_SOURCES)))

$(BUILD_DIR)/$(LDSCRIPT): $(LDSCRIPT)
	$(CC) -x assembler-with-cpp -P -E $(C_DEFS) $(C_INCLUDES) $< -o $@

$(BUILD_DIR)/%.o: %.c Makefile | $(BUILD_DIR)
	$(CC) -c $(CFLAGS) -Wa,-a,-ad,-alms=$(BUILD_DIR)/$(notdir $(<:.c=.lst)) $< -o $@

$(BUILD_DIR)/%.o: %.s Makefile | $(BUILD_DIR)
	$(AS) -c $(CFLAGS) $< -o $@

$(BUILD_DIR)/$(TARGET).elf: $(OBJECTS) $(BUILD_DIR)/$(LDSCRIPT) Makefile
ifeq ($(MAX17261_USE_WEAK), 1)
	@echo "\nMAX17261: build using weak functions\n"
else
	@echo "\nMAX17261: build using function pointers\n"
endif
	$(CC) -o $@ $(OBJECTS) $(LDFLAGS)
	$(SZ) $@

$(BUILD_DIR)/%.hex: $(BUILD_DIR)/%.elf | $(BUILD_DIR)
	$(HEX) $< $@

$(BUILD_DIR)/%.bin: $(BUILD_DIR)/%.elf | $(BUILD_DIR)
	$(BIN) $< $@

$(BUILD_DIR):
	mkdir $@

#######################################
# Program the device
#######################################
program: $(BUILD_DIR)/$(TARGET).elf
	$(OPENOCD) $(OPENOCD_FLAGS) $(OPENOCD_OPTIONS) -f "openocd.cfg" -c "program $(BUILD_DIR)/$(TARGET).elf verify reset exit"

#######################################
# clean up
#######################################
clean:
	-rm -fR $(BUILD_DIR)

#######################################
# Doxygen
#######################################
.PHONY: docs
docs:
	cd docs && doxygen

#######################################
# dependencies
#######################################
-include $(wildcard $(BUILD_DIR)/*.d)

# *** EOF ***
